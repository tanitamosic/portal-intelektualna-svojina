export class Zig {
    tipZiga: string = "";
    opisIzgledaZiga: string = "";
    drugaVrstaZnakaOpis: string = "";
    izgledPutanjaDoSlike: string = "";
    transliteracijaZnaka: string = "";
    prevodZnaka: string = "";
    opisZnaka: string = "";
    bojaConcatenated: string = "";
  
    public Zig(){
      this.tipZiga = "";
      this.opisIzgledaZiga = "";
      this.drugaVrstaZnakaOpis = "";
      this.izgledPutanjaDoSlike= "";
      this.transliteracijaZnaka = "";
      this.prevodZnaka = "";
      this.opisZnaka = "";
      this.bojaConcatenated = "";
    }
  }